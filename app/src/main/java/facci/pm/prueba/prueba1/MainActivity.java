package facci.pm.prueba.prueba1;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DownloadManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    EditText txtCedula;
    TextView lbl_id, lbl_cedula, lbl_nombres, lbl_apellidos, lbl_ciudad, lbl_titulo, jsonTextView;
    Button btnDatos, btnHoras;

    RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtCedula = findViewById(R.id.txtCedula);
        lbl_id = findViewById(R.id.lbl_id);
        lbl_cedula = findViewById(R.id.lbl_cedula);
        lbl_nombres = findViewById(R.id.lbl_nombre);
        lbl_apellidos = findViewById(R.id.lbl_apellido);
        lbl_ciudad = findViewById(R.id.lbl_ciudad);
        lbl_titulo = findViewById(R.id.lbl_titulo);
        jsonTextView = findViewById(R.id.jsonText);
        btnDatos = findViewById(R.id.btnCedula);
        btnHoras = findViewById(R.id.btnhoras);

        requestQueue = Volley.newRequestQueue(getApplicationContext());

        btnDatos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String cedulaTrabajador = txtCedula.getText().toString();
                if (!cedulaTrabajador.isEmpty()){
                    ConsultarDatosTrabajador(cedulaTrabajador);
                }else {
                    txtCedula.setError("CAMPO OBLIGATORIO");
                    txtCedula.requestFocus();
                }
            }
        });

        btnHoras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String cedulaTrabajador = txtCedula.getText().toString();
                if (!cedulaTrabajador.isEmpty()){
                    ConsultarHorasTrabajador(cedulaTrabajador);
                }else {
                    txtCedula.setError("CAMPO OBLIGATORIO");
                    txtCedula.requestFocus();
                }
            }
        });
    }


    private void ConsultarDatosTrabajador(String cedulaTrabajador) {

        String url = "http://backend-posts.herokuapp.com/operative/" +cedulaTrabajador;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    lbl_id.setText("ID: " + response.getString("_id"));
                    lbl_cedula.setText("CEDULA: " + response.get("cedula"));
                    lbl_nombres.setText("NOMBRE:  " + response.getString("nombres"));
                    lbl_apellidos.setText("APELLIDOS: " + response.getString("apellidos"));
                    lbl_ciudad.setText("CIUDAD: " + response.getString("ciudad"));
                    lbl_titulo.setText("TITULO: " +  response.getString("titulo"));
                }catch (Exception e){
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        requestQueue.add(jsonObjectRequest);

    }

    private void ConsultarHorasTrabajador(String cedulaTrabajador) {
        String url = "http://backend-posts.herokuapp.com/checkin/" +cedulaTrabajador;
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {

                    for (int i = 0; i<response.length(); i++){
                        JSONObject jsonObject = response.getJSONObject(i);
                        String content = "";
                        //Log.e("array", jsonObject.toString());
                        //content += "_id: "+ jsonObject.getString("_id") + "\n";
                        content += "cedula: "+  jsonObject.getString("cedula") + "\n";
                        content += "fecha: "+ jsonObject.getString("fecha")  + "\n";
                        content += "hora: "+ jsonObject.getString("hora")  + "\n";
                        content += "minuto: "+ jsonObject.getString("minuto")  + "\n";
                        content += "segundo: "+ jsonObject.getString("fecha")  + "\n\n";
                        jsonTextView.append(content);
                    }

                }catch (Exception e){
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        requestQueue.add(jsonArrayRequest);

    }
}
